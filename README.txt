# REST Error Message #

Extension for the Services module returning a JSON formatted error message instead of a WSOD with error info only through the HTTP header. In many cases this is needed by the application making the request. At the same time it outputs human readable output, hinting developers directly what is going on in the browser.


## Some examples ##

### Using a fallback when no message has been set ###

{
    "error": {
        "code": 404,
        "message": "Not Found"
    }
}
### Respecting the services_error() set in the Services module ###

{
    "error": {
        "code": 401,
        "message": "Unauthorized : Missing required argument custom_query_parameter"
    }
}
### Respecting the services_error() set by a custom service API ###

{
    "error": {
        "code": 404,
        "message": "Not found : Published content api_custom_content _type with nid 494165 not found"
    }
}

## It also writes a log message ##

  watchdog('services', 'REST service execute error with status code %code - %message', array(
    '%code' => $error_alter_array['code'],
    '%message' => $message,
  ), WATCHDOG_ERROR);


## Usage ##

Any code and message set with the function services_error($message, $code = 0, $data = NULL) by the Services module or in a custom [YOUR-SERVICE].service.inc file is respected. That way you can provide API specific error data for your service.


## List of supported fallback codes and messages if no message is found ##

Based on https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html.

code: 100, message: 'Continue'
code: 101, message: 'Switching Protocols'
// Note: 200 is the code for OK but when it arrives here something went wrong.
code: 200, message: 'Something went wrong'
code: 201, message: 'Created'
code: 202, message: 'Accepted'
code: 203, message: 'Non-Authoritative Information'
code: 204, message: 'No Content'
code: 205, message: 'Reset Content'
code: 206, message: 'Partial Content'
code: 300, message: 'Multiple Choices'
code: 301, message: 'Moved Permanently'
code: 302, message: 'Moved Temporarily'
code: 303, message: 'See Other'
code: 304, message: 'Not Modified'
code: 305, message: 'Use Proxy'
code: 400, message: 'Bad Request'
code: 401, message: 'Unauthorized'
code: 402, message: 'Payment Required'
code: 403, message: 'Forbidden'
code: 404, message: 'Not Found'
code: 405, message: 'Method Not Allowed'
code: 406, message: 'Not Acceptable'
code: 407, message: 'Proxy Authentication Required'
code: 408, message: 'Request Time-out'
code: 409, message: 'Conflict'
code: 410, message: 'Gone'
code: 411, message: 'Length Required'
code: 412, message: 'Precondition Failed'
code: 413, message: 'Request Entity Too Large'
code: 414, message: 'Request-URI Too Large'
code: 415, message: 'Unsupported Media Type'
code: 500, message: 'Internal Server Error'
code: 501, message: 'Not Implemented'
code: 502, message: 'Bad Gateway'
code: 503, message: 'Service Unavailable'
code: 504, message: 'Gateway Time-out'
code: 505, message: 'HTTP Version not supported'
code: ???, message: 'Unknown http status code'